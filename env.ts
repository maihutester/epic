// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` withs`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    domain: "google.com"
    apiKey: "AIzaSyAlUBfT0vChMxgIzoy7G-9Tuv2r1D_QYcs"
    authDomain: "fitness-tracker-31e15.firebaseapp.com",
    databaseURL: "https://fitness-tracker-31e15.firebaseio.com",
    projectId: "fitness-tracker-31e15",
    storageBucket: "fitness-tracker-31e15.appspot.com",
    messagingSenderId: "909259679192"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.